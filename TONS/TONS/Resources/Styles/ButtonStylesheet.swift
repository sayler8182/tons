//
//  ButtonStylesheet.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import Fashion

enum ButtonStyle: String, StringConvertible {
    
    // common
    case primary
    
    var string: String {
        return self.rawValue
    }
}

final class ButtonStylesheet: Stylesheet {
    func define() {
        
        // primary
        self.register(ButtonStyle.primary) { (button: UIButton) in
            button.backgroundColor = UIColor.primary
            button.setTitleColor(UIColor.white, for: UIControlState.normal)
            button.titleLabel?.font = UIFont.defaultSemiBoldFont(size: 16)
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
    }
}
