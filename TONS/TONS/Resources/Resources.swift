//
//  Resources.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import ChameleonFramework

// MARK: UIColor
extension UIColor {
    static var primary: UIColor         = UIColor(hexString: "e73656")!
    static var secondary: UIColor       = UIColor(hexString: "70cabd")!
    static var separator: UIColor       = UIColor(hexString: "ecebec")!
    static var typographyDark: UIColor  = UIColor(hexString: "474752")!
    static var typographyLight: UIColor = UIColor(hexString: "a29d9f")!
    static var popupBackground: UIColor = UIColor.black.withAlphaComponent(0.4)
}

// MARK: Font
extension UIFont {
    static func defaultRegularFont(size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.regular)
    }
    static func defaultSemiBoldFont(size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
    }
}
