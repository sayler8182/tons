//
//  ProductsAdapter.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import UIKit

protocol ProductsAdapterDelegate: class {
    func loadNewPage(sender: ProductsAdapterProtocol)
}

protocol ProductsAdapterProtocol: AdapterProtocol {
    var delegate: ProductsAdapterDelegate?                      { get set }
    var products: [ProductsItemPresenterProtocol]               { get }
    var page: Int                                               { get set }
    
    func set(products: [ProductsItemPresenterProtocol])
    func append(products: [ProductsItemPresenterProtocol])
    func updatingFailed()
}

extension ProductsAdapter {
    enum ViewState {
        case normal
        case waitingForPage
        case endOfPages
    }
}

class ProductsAdapter: Adapter, ProductsAdapterProtocol {
    weak var delegate: ProductsAdapterDelegate?
    fileprivate (set) var products: [ProductsItemPresenterProtocol]      = []
    var page: Int = 0
    fileprivate var viewState: ViewState = ViewState.normal
    
    // set products
    func set(products: [ProductsItemPresenterProtocol]) {
        self.page = 1
        self.products = products
        self.set(presenters: self.products)
        self.reload()
        self.viewState = products.count == 0 ? ViewState.endOfPages : ViewState.normal
    }
    
    // append products
    func append(products: [ProductsItemPresenterProtocol]) {
        self.page += 1
        self.products += products
        self.insert(presenters: products)
        self.viewState = products.count == 0 ? ViewState.endOfPages : ViewState.normal
    }
    
    // updating failed
    func updatingFailed() {
        self.viewState = ViewState.normal
    }
}

// MARK: UIScrollViewDelegate
extension ProductsAdapter {
    
    // scroll did scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var newOffset = scrollView.contentOffset
        newOffset.y += scrollView.contentInset.top
        
        // load next page
        if self.viewState == ViewState.normal {
            let y: CGFloat = scrollView.contentOffset.y + scrollView.bounds.size.height - scrollView.contentInset.bottom;
            let h: CGFloat = scrollView.contentSize.height
            let reloadDistance: CGFloat = -1000.0
            
            if ( (h != 0) && (y > h + reloadDistance)) {
                self.viewState = ViewState.waitingForPage
                
                // ask for new page
                self.delegate?.loadNewPage(sender: self)
            }
        }
    }
}
