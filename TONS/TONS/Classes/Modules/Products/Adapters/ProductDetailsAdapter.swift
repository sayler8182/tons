//
//  ProductDetailsAdapter.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import UIKit

protocol ProductDetailsAdapterProtocol: AdapterProtocol {
}

class ProductDetailsAdapter: Adapter, ProductDetailsAdapterProtocol {

}
