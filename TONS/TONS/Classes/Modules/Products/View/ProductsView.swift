//
//  ProductsView.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit
import Lottie
 
protocol ProductsViewProtocol: ViewProtocol {
    var presenter: ProductsPresenterProtocol! { get set }
    
    func endRefreshing()
    func presentError(message: String?)
}

extension ProductsView {
    enum ViewState {
        case normal
        case refreshing
        case searchRefreshing
        case `init`
    }
}

class ProductsView: ViewController, ProductsViewProtocol {
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.register(type: ProductCollectionViewCell.self)
            self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
            self.collectionView.insertSubview(self.refreshControl, at: 0)
            self.collectionView.set(adapter: self.presenter.productsAdapter)
        }
    }
    @IBOutlet weak var loaderParentView: UIView! {
        didSet {
            self.loaderParentView.alpha = 0
            self.loaderParentView.backgroundColor = UIColor.popupBackground
        }
    }
    @IBOutlet weak var loaderView: LOTAnimationView! {
        didSet {
            self.loaderView.layer.cornerRadius = 16
            self.loaderView.contentMode = UIViewContentMode.scaleAspectFit
            self.loaderView.autoReverseAnimation = true
            self.loaderView.loopAnimation = true
            
            let layer: CALayer = self.loaderView.layer
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 0.2
            layer.shadowOffset = CGSize(width: 1, height: 1)
            layer.shadowRadius = 3
            layer.shadowPath = UIBezierPath(roundedRect: self.loaderView.bounds, cornerRadius: 16).cgPath
        }
    }
    
    @IBOutlet weak var errorViewHeightConstraint: NSLayoutConstraint!
    
    fileprivate lazy var searchBar: UISearchBar = {
        let searchBar: UISearchBar = UISearchBar(frame: CGRect.zero)
        searchBar.placeholder = "Search..."
        searchBar.delegate = self
        searchBar.sizeToFit()
        return searchBar
    }()
    
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl: UIRefreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.primary
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    fileprivate var queryTimer: Timer? = nil
    fileprivate var errorTimer: Timer? = nil
    fileprivate var _viewState: ViewState = ViewState.normal
    fileprivate var viewState: ViewState {
        return self._viewState
    }
    override var lifeCyclePresenter: PresenterProtocol? { return self.presenter }
    var presenter: ProductsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set default state
        self.setViewState(viewState: ViewState.init, animated: true)
        
        // append search bar
        let barButton = UIBarButtonItem(customView: self.searchBar)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // dismiss keyboard
        self.searchBar.resignFirstResponder()
    }
    
    @objc func handleRefresh(_ sender: UIRefreshControl) {
        self.collectionView.panGestureRecognizer.isEnabled = false
        self.collectionView.panGestureRecognizer.isEnabled = true
        self.setViewState(viewState: ViewState.refreshing, animated: true)
        
        let query: String? = self.searchBar.text
        self.presenter.reloadProducts(query: query)
    }
    
    // end refreshing
    func endRefreshing() {
        self.refreshControl.endRefreshing()
        self.setViewState(viewState: ViewState.normal, animated: true)
    }
    
    // present error belove table view
    func presentError(message: String?) {
        self.errorTimer?.invalidate()
        self.errorLabel.text = message
        self.errorTimer = Timer.scheduledTimer(withTimeInterval: 4, repeats: false, block: { [weak self] (_) in
            guard let `self` = self else { return }
            self.errorViewHeightConstraint.constant = 0
            UIView.animate(
                withDuration: 0.2,
                animations: self.view.layoutIfNeeded)
        })
        
        self.errorViewHeightConstraint.constant = 20
        UIView.animate(
            withDuration: 0.2,
            animations: self.view.layoutIfNeeded)
    }
    
    // set view state
    fileprivate func setViewState(viewState: ViewState, animated: Bool) {
        self._viewState = viewState
        let time: TimeInterval = animated ? 0.2 : 0
        switch self.viewState {
        case .normal,
             .refreshing:
            self.loaderView.stop()
            UIView.animate(
                withDuration: time,
                animations: {
                    self.loaderParentView.alpha = 0
            })
        case .searchRefreshing,
             .init:
            self.collectionView.setContentOffset(CGPoint.zero, animated: true)
            self.loaderView.setAnimation(named: "progress")
            self.loaderView.play()
            UIView.animate(
                withDuration: time,
                animations: {
                    self.loaderParentView.alpha = 1
            })
        }
    }
}

// MARK: UISearchBarDelegate
extension ProductsView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.queryTimer?.invalidate()
        self.queryTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] (_) in
            guard let `self` = self else { return }
            guard self.viewState == ViewState.normal else { return }
            self.queryTimer?.invalidate()
            self.setViewState(viewState: ViewState.searchRefreshing, animated: true)
            let query: String? = self.searchBar.text
            self.presenter.reloadProducts(query: query)
        })
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
}
