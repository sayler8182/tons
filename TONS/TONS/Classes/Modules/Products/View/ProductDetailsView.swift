//
//  ProductDetailsView.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit
import Lottie

protocol ProductDetailsViewProtocol: ViewProtocol {
    var presenter: ProductDetailsPresenterProtocol! { get set }
}

class ProductDetailsView: ViewController, ProductDetailsViewProtocol {
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.register(type: ProductDetailsImagesTableViewCell.self)
            self.tableView.register(type: ProductDetailsSectionTableViewCell.self)
            self.tableView.register(type: ProductDetailsTextTableViewCell.self)
            self.tableView.register(type: ProductDetailsItemTableViewCell.self)
            self.tableView.set(adapter: self.presenter.productDetailsAdapter)
        }
    }
    
    override var lifeCyclePresenter: PresenterProtocol? { return self.presenter }
    var presenter: ProductDetailsPresenterProtocol!
    
    @IBAction func backClick(_ sender: UIBarButtonItem) {
        self.presenter.goBack()
    }
}
