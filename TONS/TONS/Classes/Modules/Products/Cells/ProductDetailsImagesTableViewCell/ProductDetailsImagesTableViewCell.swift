//
//  ProductDetailsImagesTableViewCell.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit
import Lottie

protocol ProductDetailsImagesItemPresenterProtocol: ItemPresenter {
    var imageCacheManager: ImageCacheManagerProtocol { get }
}

class ProductDetailsImagesItemPresenter: ProductDetailsImagesItemPresenterProtocol {
    var id: String              = ProductDetailsImagesTableViewCell.reusableIdentifier
    var model: ItemModel?
    
    let imageCacheManager: ImageCacheManagerProtocol
    
    init(model: ItemModel?,
         imageCacheManager: ImageCacheManagerProtocol) {
        self.model = model
        self.imageCacheManager = imageCacheManager
    }
    
    func size(for list: UIList, model: ItemModel?) -> CGSize {
        let width: CGFloat = list.bounds.width
        let height: CGFloat = width * 0.75
        return CGSize(width: width, height: height)
    }
    
    func configure(item: UIView) {
        let item = item as! ProductDetailsImagesTableViewCell
        let model = self.model as? Product
        
        // image
        let imagePath: String? = model?.images?.first
        if imagePath?.isEmpty == false {
            item.startLoading(animated: true)
            let scale: ImageScale = ImageScale.fill(size: item.productImageView.bounds.size)
            self.imageCacheManager.setImage(
                imageView: item.productImageView,
                path: imagePath,
                scale: scale,
                completion: { [weak item] (view, image) in
                    view.image = image ?? UIImage(named: "product_placeholder")
                    item?.stopLoading(animated: true)
            })
        } else {
            item.productImageView.image = UIImage(named: "product_placeholder")
            item.productImageView.alpha = 1
        }
    }
    
    func selected() { }
}

class ProductDetailsImagesTableViewCell: TableViewCell {
    @IBOutlet weak var productImageView: UIImageView! {
        didSet { self.productImageView.alpha = 0 }
    }
    @IBOutlet weak var progressView: LOTAnimationView! {
        didSet {
            self.progressView.alpha = 0
            self.progressView.setAnimation(named: "progress")
            self.progressView.contentMode = UIViewContentMode.scaleAspectFit
            self.progressView.autoReverseAnimation = true
            self.progressView.loopAnimation = true
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.productImageView.image = nil
        self.productImageView.alpha = 0
        self.progressView.alpha = 0
    }
    
    // start loading
    func startLoading(animated: Bool = false) {
        self.progressView.play()
        let time: TimeInterval = animated ? 0.2 : 0
        UIView.animate(
            withDuration: time,
            animations: {
                self.productImageView.alpha = 0
                self.progressView.alpha = 1
        })
    }
    
    // stop loading
    func stopLoading(animated: Bool = false) {
        self.progressView.stop()
        let time: TimeInterval = animated ? 0.2 : 0
        UIView.animate(
            withDuration: time,
            animations: {
                self.productImageView.alpha = 1
                self.progressView.alpha = 0
        })
    }
}
