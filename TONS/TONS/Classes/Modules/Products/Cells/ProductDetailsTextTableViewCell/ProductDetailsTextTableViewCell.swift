//
//  ProductDetailsTextTableViewCell.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol ProductDetailsTextItemPresenterProtocol: ItemPresenter { }

class ProductDetailsTextItemPresenter: ProductDetailsTextItemPresenterProtocol {
    var id: String              = ProductDetailsTextTableViewCell.reusableIdentifier
    var model: ItemModel?
    
    init(model: ItemModel?) {
        self.model = model
    }
     
    func size(for list: UIList, model: ItemModel?) -> CGSize {
        let width: CGFloat = list.bounds.width
        var height: CGFloat = 44
        
        // calculate text height
        let text: String? = model as? String
        let font: UIFont = UIFont.defaultRegularFont(size: 14)
        let textHeight: CGFloat = text?.height(width: width - 16, font: font) ?? 0
        height = max(height, textHeight + 16)
        
        return CGSize(width: width, height: height)
    }
    
    func configure(item: UIView) {
        let item = item as! ProductDetailsTextTableViewCell
        let model = self.model as? String
        
        item.detailsLabel.text = model
    }
    
    func selected() { }
}

class ProductDetailsTextTableViewCell: TableViewCell {
    @IBOutlet weak var detailsLabel: UILabel! {
        didSet {
            self.detailsLabel.font = UIFont.defaultRegularFont(size: 14)
            self.detailsLabel.numberOfLines = 0
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.detailsLabel.text = nil
    }
}
