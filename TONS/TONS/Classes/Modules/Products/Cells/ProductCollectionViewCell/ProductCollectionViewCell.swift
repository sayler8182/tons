//
//  ProductCollectionViewCell.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit
import ChameleonFramework
import Lottie

protocol ProductsItemPresenterDelegate: class {
    func productSelected(sender: ProductsItemPresenterProtocol)
}

protocol ProductsItemPresenterProtocol: ItemPresenter {
    var imageCacheManager: ImageCacheManagerProtocol { get }
}

class ProductsItemPresenter: ProductsItemPresenterProtocol {
    var id: String              = ProductCollectionViewCell.reusableIdentifier
    var model: ItemModel?
    
    weak var delegate: ProductsItemPresenterDelegate?
    let imageCacheManager: ImageCacheManagerProtocol
    
    init(model: ItemModel?,
         delegate: ProductsItemPresenterDelegate,
         imageCacheManager: ImageCacheManagerProtocol) {
        self.model = model
        self.delegate = delegate
        self.imageCacheManager = imageCacheManager
    }
    
    func size(for list: UIList, model: ItemModel?) -> CGSize {
        let width: CGFloat = floor(list.bounds.width / 2)
        let height: CGFloat = width
        return CGSize(width: width, height: height)
    }
    
    func configure(item: UIView) {
        let item = item as! ProductCollectionViewCell
        let model = self.model as? Product
        item.brandNameLabel.text = model?.brandName
        item.nameLabel.text = model?.name
        item.sizeLabel.text = model?.size
        item.sizeView.isHidden = model?.size?.isEmpty != false
        
        // image
        let imagePath: String? = model?.images?.first
        if imagePath?.isEmpty == false {
            item.startLoading(animated: true)
            let scale: ImageScale = ImageScale.fill(size: item.productImageView.bounds.size)
            self.imageCacheManager.setImage(
                imageView: item.productImageView,
                path: imagePath,
                scale: scale,
                completion: { [weak item] (view, image) in
                    view.image = image ?? UIImage(named: "product_placeholder")
                    item?.stopLoading(animated: true)
            })
        } else {
            item.productImageView.image = UIImage(named: "product_placeholder")
            item.productImageView.alpha = 1
        }
    }
    
    func selected() {
        self.delegate?.productSelected(sender: self)
    }
}

class ProductCollectionViewCell: CollectionViewCell {
    private let borderRadius: CGFloat = 2
    @IBOutlet weak var borderView: UIView! {
        didSet { self.layer.cornerRadius = self.borderRadius }
    }
    @IBOutlet weak var productImageView: UIImageView! {
        didSet { self.productImageView.alpha = 0 }
    }
    @IBOutlet weak var progressView: LOTAnimationView! {
        didSet {
            self.progressView.alpha = 0
            self.progressView.setAnimation(named: "progress")
            self.progressView.contentMode = UIViewContentMode.scaleAspectFit
            self.progressView.autoReverseAnimation = true
            self.progressView.loopAnimation = true
        }
    }
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var sizeView: UIView! {
        didSet {
            self.sizeView.isHidden = true
            self.sizeView.layer.cornerRadius = self.sizeView.bounds.height / 2
        }
    }
    @IBOutlet weak var sizeLabel: UILabel! {
        didSet { self.sizeLabel.font = UIFont.defaultSemiBoldFont(size: 14) }
    }
    @IBOutlet weak var brandNameLabel: UILabel! {
        didSet {
            self.brandNameLabel.font = UIFont.defaultRegularFont(size: 11)
            self.brandNameLabel.textColor = UIColor.typographyLight
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            self.nameLabel.font = UIFont.defaultSemiBoldFont(size: 14)
            self.nameLabel.textColor = UIColor.typographyDark
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // refresh view
        self.refreshShadow()
        self.refreshGradient()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.productImageView.image = nil
        self.productImageView.alpha = 0
        self.progressView.alpha = 0
        self.sizeView.isHidden = true
        self.brandNameLabel.text = nil
        self.nameLabel.text = nil
    }
    
    // start loading
    func startLoading(animated: Bool = false) {
        self.progressView.play()
        let time: TimeInterval = animated ? 0.2 : 0
        UIView.animate(
            withDuration: time,
            animations: {
                self.productImageView.alpha = 0
                self.progressView.alpha = 1
        })
    }
    
    // stop loading
    func stopLoading(animated: Bool = false) {
        self.progressView.stop()
        let time: TimeInterval = animated ? 0.2 : 0
        UIView.animate(
            withDuration: time,
            animations: {
                self.productImageView.alpha = 1
                self.progressView.alpha = 0
        })
    }
    
    // refresh shadow
    private func refreshShadow() {
        let view: UIView = self.borderView
        let layer: CALayer = view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowRadius = 3
        layer.shadowPath = UIBezierPath(roundedRect: view.bounds, cornerRadius: self.borderRadius).cgPath
    }
    
    // refresh gradient
    private func refreshGradient() {
        let frame: CGRect = self.gradientView.bounds
        self.gradientView.backgroundColor = UIColor(
            gradientStyle: UIGradientStyle.topToBottom,
            withFrame: frame,
            andColors: [
                UIColor.white.withAlphaComponent(0),
                UIColor.white.withAlphaComponent(0.95)
            ])
    }
}
