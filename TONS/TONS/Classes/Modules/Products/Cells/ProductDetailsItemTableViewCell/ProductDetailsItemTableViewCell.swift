//
//  ProductDetailsItemTableViewCell.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol ProductDetailsItemPresenterProtocol: ItemPresenter { }

class ProductDetailsItemPresenter: ProductDetailsItemPresenterProtocol {
    var id: String              = ProductDetailsItemTableViewCell.reusableIdentifier
    var model: ItemModel?
    
    init(model: ItemModel?) {
        self.model = model
    }
    
    func size(for list: UIList, model: ItemModel?) -> CGSize {
        let width: CGFloat = list.bounds.width
        let height: CGFloat = 32
        return CGSize(width: width, height: height)
    }
    
    func configure(item: UIView) {
        let item = item as! ProductDetailsItemTableViewCell
        let model = self.model as? ProductDetailsItem
        
        item.typeLabel.text = model?.type.name
        item.valueLabel.text = model?.value.string
    }
    
    func selected() { }
}

class ProductDetailsItemTableViewCell: TableViewCell {
    @IBOutlet weak var typeLabel: UILabel! {
        didSet { self.typeLabel.font = UIFont.defaultSemiBoldFont(size: 14) }
    }
    @IBOutlet weak var valueLabel: UILabel! {
        didSet { self.valueLabel.font = UIFont.defaultRegularFont(size: 14) }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.typeLabel.text = nil
        self.valueLabel.text = nil
    }
}
