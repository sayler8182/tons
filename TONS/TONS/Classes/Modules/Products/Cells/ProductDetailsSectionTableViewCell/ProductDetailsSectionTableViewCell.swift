//
//  ProductDetailsSectionTableViewCell.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol ProductDetailsSectionItemPresenterProtocol: ItemPresenter { }

class ProductDetailsSectionItemPresenter: ProductDetailsSectionItemPresenterProtocol {
    var id: String              = ProductDetailsSectionTableViewCell.reusableIdentifier
    var model: ItemModel?
    
    init(model: ItemModel?) {
        self.model = model
    }
    
    func size(for list: UIList, model: ItemModel?) -> CGSize {
        let width: CGFloat = list.bounds.width
        let height: CGFloat = 44
        return CGSize(width: width, height: height)
    }
    
    func configure(item: UIView) {
        let item = item as! ProductDetailsSectionTableViewCell
        let model = self.model as? String
        
        item.titleLabel.text = model
    }
    
    func selected() { }
}

class ProductDetailsSectionTableViewCell: TableViewCell {
    @IBOutlet weak var titleLabel: UILabel! {
        didSet { self.titleLabel.font = UIFont.defaultSemiBoldFont(size: 16) }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.titleLabel.text = nil
    }
}
