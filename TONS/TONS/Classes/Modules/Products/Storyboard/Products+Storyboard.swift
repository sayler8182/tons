//
//  Products+Storyboard.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

extension Storyboards {
    struct Products: Storyboard {
        static let identifier = "Products"
        
        // products view
        static func productsView() -> UIViewController {
            let view: ProductsViewProtocol = self.instantiateViewController(withIdentifier: "Products") as ProductsView
            let productsAdapter: ProductsAdapterProtocol = ProductsAdapter()
            let presenter: ProductsPresenterProtocol = ProductsPresenter(
                view: view,
                productsRouter: self.productsRouter(view: view),
                productsAdapter: productsAdapter,
                productsInteractor: self.productsInteractor())
            view.presenter = presenter
            productsAdapter.delegate = presenter
            return view.asController
        }
        
        // product details view
        static func productDetailsView(product: Product) -> UIViewController {
            let view: ProductDetailsViewProtocol = self.instantiateViewController(withIdentifier: "ProductDetails") as ProductDetailsView
            let productDetailsAdapter: ProductDetailsAdapterProtocol = ProductDetailsAdapter()
            let presenter: ProductDetailsPresenterProtocol = ProductDetailsPresenter(
                view: view,
                productDetailsAdapter: productDetailsAdapter,
                productsInteractor: self.productsInteractor(),
                product: product)
            view.presenter = presenter
            return view.asController
        }
    }
}
