//
//  ProductsPresenter.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol ProductsPresenterProtocol: PresenterProtocol, ProductsAdapterDelegate {
    init(view: ProductsViewProtocol,
         productsRouter: ProductsRouterProtocol,
         productsAdapter: ProductsAdapterProtocol,
         productsInteractor: ProductsInteractorProtocol)
    
    var productsAdapter: ProductsAdapterProtocol { get }
    
    func reloadProducts()
    func reloadProducts(query: String?)
}

class ProductsPresenter: ProductsPresenterProtocol {
    var statusBarStyle: UIStatusBarStyle? = UIStatusBarStyle.default
    fileprivate var productsRouter: ProductsRouterProtocol
    fileprivate weak var view: ProductsViewProtocol!
    fileprivate (set) var productsAdapter: ProductsAdapterProtocol
    fileprivate var productsInteractor: ProductsInteractorProtocol
    fileprivate var query: String? = nil
    
    required init(view: ProductsViewProtocol,
                  productsRouter: ProductsRouterProtocol,
                  productsAdapter: ProductsAdapterProtocol,
                  productsInteractor: ProductsInteractorProtocol) {
        self.view = view
        self.productsRouter = productsRouter
        self.productsAdapter = productsAdapter
        self.productsInteractor = productsInteractor
    }
    
    func viewDidLoad() {
        
        // loads first page
        self.loadProducts(isAppending: false)
    }
    
    // reload products
    func reloadProducts() {
        self.reloadProducts(query: nil)
    }
    
    // reload products
    func reloadProducts(query: String?) {
        self.query = query
        self.productsAdapter.page = 0
        self.loadProducts(isAppending: false)
    }
    
    // load products
    fileprivate func loadProducts(isAppending: Bool) {
        let query: String? = self.query
        let newPage: Int = self.productsAdapter.page + 1
        self.productsInteractor.products(
            query: query,
            page: newPage,
            success: { [weak self] (products) in
                guard let `self` = self else { return }
                self.updateProducts(products: products, isAppending: isAppending)
                self.view.endRefreshing()
        }, error: { [weak self] (message) in
            guard let `self` = self else { return }
            self.productsAdapter.updatingFailed()
            self.view.presentError(message: message)
            self.view.endRefreshing()
        })
    }
    
    // update products
    fileprivate func updateProducts(products: [Product], isAppending: Bool) {
        let productsPresenters: [ProductsItemPresenterProtocol] = products
            .map {
                ProductsItemPresenter(
                    model: $0,
                    delegate: self,
                    imageCacheManager: ImageCacheManager())
        }
        if isAppending {
            self.productsAdapter.append(products: productsPresenters)
        } else {
            self.productsAdapter.set(products: productsPresenters)
        }
    }
}

// MARK: ProductsAdapterDelegate
extension ProductsPresenter {
    func loadNewPage(sender: ProductsAdapterProtocol) {
        self.loadProducts(isAppending: true)
    }
}

// MARK: ProductsItemPresenterDelegate
extension ProductsPresenter: ProductsItemPresenterDelegate {
    func productSelected(sender: ProductsItemPresenterProtocol) {
        
        // show product details
        guard let product = sender.model as? Product else { return }
        self.productsRouter.pushProductDetails(product: product)
    }
}
