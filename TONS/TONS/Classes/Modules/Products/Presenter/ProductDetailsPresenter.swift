//
//  ProductDetailsPresenter.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol ProductDetailsPresenterProtocol: PresenterProtocol {
    init(view: ProductDetailsViewProtocol,
         productDetailsAdapter: ProductDetailsAdapterProtocol,
         productsInteractor: ProductsInteractorProtocol,
         product: Product)
    
    var productDetailsAdapter: ProductDetailsAdapterProtocol { get }
    
    func goBack()
}

class ProductDetailsPresenter: ProductDetailsPresenterProtocol {
    var statusBarStyle: UIStatusBarStyle? = UIStatusBarStyle.default
    fileprivate weak var view: ProductDetailsViewProtocol!
    fileprivate (set) var productDetailsAdapter: ProductDetailsAdapterProtocol
    fileprivate var productsInteractor: ProductsInteractorProtocol
    
    required init(view: ProductDetailsViewProtocol,
                  productDetailsAdapter: ProductDetailsAdapterProtocol,
                  productsInteractor: ProductsInteractorProtocol,
                  product: Product) {
        self.view = view
        self.productDetailsAdapter = productDetailsAdapter
        self.productsInteractor = productsInteractor
        
        // load product details
        var presenters: [ItemPresenter] = []
        
        // images
        presenters.append(ProductDetailsImagesItemPresenter(
                model: product,
                imageCacheManager: ImageCacheManager()))
        
        // section 1
        presenters.append(ProductDetailsSectionItemPresenter(
            model: product.gtin14))
        
        // full name
        let fullName: String = [
            product.brandName,
            product.name
            ].compactMap { $0 }
        .joined(separator: " ")
        presenters.append(ProductDetailsTextItemPresenter(
            model: fullName))
        
        // section 2
        presenters.append(ProductDetailsSectionItemPresenter(
            model: "Details"))
        
        // details
        for item in product.details {
            presenters.append(ProductDetailsItemPresenter(
                model: item))
        }
        
        // set presenters
        self.productDetailsAdapter.set(presenters: presenters)
    }
    
    // go back
    func goBack() {
        let controller: UIViewController = self.view.asController
        controller.navigationController?.popViewController(animated: true)
    }
}
