//
//  InitPresenter.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol InitPresenterProtocol: PresenterProtocol {
    init(view: InitViewProtocol,
         productsRouter: ProductsRouterProtocol)
    
    func showDefaultScreen()
}

class InitPresenter: InitPresenterProtocol {
    var statusBarStyle: UIStatusBarStyle? = nil
    fileprivate weak var view: InitViewProtocol!
    fileprivate var productsRouter: ProductsRouterProtocol
    
    required init(view: InitViewProtocol,
                  productsRouter: ProductsRouterProtocol) {
        self.view = view
        self.productsRouter = productsRouter
    }
    
    // show default screen
    func showDefaultScreen() {
        // This func can switch between default screens
        // e.g when user isn't logged display login screen otherwise show products
        self.productsRouter.presentProducts()
    }
}
