//
//  InitView.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit
import Lottie

protocol InitViewProtocol: ViewProtocol {
    var presenter: InitPresenterProtocol! { get set }
}

class InitView: ViewController, InitViewProtocol {
    override var lifeCyclePresenter: PresenterProtocol? { return self.presenter }
    var presenter: InitPresenterProtocol!
    
    @IBOutlet weak var confettiView: LOTAnimationView! {
        didSet { self.confettiView.contentMode = UIViewContentMode.scaleAspectFit }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.alpha = 0
        self.playInitAnimation()
    }
    
    // play init animation
    fileprivate func playInitAnimation() {
        UIView.animate(
            withDuration: 0.2,
            animations: { [weak self] in
                guard let `self` = self else { return }
                self.view.alpha = 1
            }, completion: { [weak self] (_) in
                guard let `self` = self else { return }
                self.confettiView.setAnimation(named: "lottie_confetti")
                self.confettiView.play(completion: { [weak self] (_) in
                    guard let `self` = self else { return }
                    self.presenter.showDefaultScreen()
                })
        })
    }
}
