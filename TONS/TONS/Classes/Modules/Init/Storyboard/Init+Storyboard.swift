//
//  Init+Storyboard.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

extension Storyboards {
    struct Init: Storyboard {
        static let identifier = "Init"
        
        // init view
        static func initView() -> UIViewController {
            let view: InitViewProtocol = self.instantiateViewController(withIdentifier: "Init") as InitView
            let presenter: InitPresenterProtocol = InitPresenter(
                view: view,
                productsRouter: self.productsRouter(view: view))
            view.presenter = presenter
            return view.asController
        }
    }
}
