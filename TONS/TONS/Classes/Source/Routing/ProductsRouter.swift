//
//  ProductsRouter.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import UIKit
import Hero

protocol ProductsRouterProtocol: RouterProtocol {
    func presentProducts()
    func pushProductDetails(product: Product) 
}

class ProductsRouter: ProductsRouterProtocol {
    weak var view: ViewProtocol!
    
    required init(view: ViewProtocol!) {
        self.view = view
    }
    
    func presentProducts() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.clear
        let windowController: UIViewController = UIViewController()
        let rootViewController: NavigationController = NavigationController()
        let controller: UIViewController = Storyboards.Products.productsView()
        rootViewController.pushViewController(controller, animated: false)
        
        window.rootViewController = windowController
        window.makeKeyAndVisible()
        AppDelegate.shared.appWindow = window
        windowController.present(rootViewController, animated: true) {
            AppDelegate.shared.initWindow?.resignKey()
            AppDelegate.shared.initWindow?.isHidden = true
        }
    }
    
    func pushProductDetails(product: Product) {
        let rootController: UIViewController = self.view.asController
        let rootNavigationController: UINavigationController? = rootController.navigationController
        let controller: UIViewController = Storyboards.Products.productDetailsView(product: product)
        controller.modalPresentationStyle = .overFullScreen
        rootNavigationController?.hero.navigationAnimationType =
            .selectBy(
                presenting: .slide(direction: .left),
                dismissing: .slide(direction: .right))
        rootNavigationController?.pushViewController(controller, animated: true)
    }
}

