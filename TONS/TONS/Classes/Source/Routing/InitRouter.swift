//
//  InitRouter.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol InitRouterProtocol: RouterProtocol {
    func presentInit()
}

class InitRouter: InitRouterProtocol {
    weak var view: ViewProtocol!
    
    init() { }
    required init(view: ViewProtocol!) {
        self.view = view
    }
    
    func presentInit() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.black
        let controller: UIViewController = Storyboards.Init.initView()
        let navigationController: UINavigationController = UINavigationController(rootViewController: controller)
        navigationController.isNavigationBarHidden = true
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        AppDelegate.shared.initWindow = window
    }
}
