//
//  ImageCacheManager.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import AlamofireImage

enum ImageScale {
    case fill(size: CGSize)
    case fit(size: CGSize)
    
    public func scale(image: UIImage?) -> UIImage? {
        switch self {
        case .fill(let size):
            return image?.af_imageAspectScaled(toFill: size)
        case .fit(let size):
            return image?.af_imageAspectScaled(toFit: size)
        }
    }
    
    public var size: CGSize {
        switch self {
        case ImageScale.fill(let size): return size
        case ImageScale.fit(let size): return size
        }
    }
}

protocol ImageCacheManagerProtocol {
    func setImage(
        imageView: UIImageView,
        path: String?,
        scale: ImageScale?,
        progress: ((_ done: Int64, _ total: Int64) -> Void)?,
        completion: ((_ view: UIImageView, _ image: UIImage?) -> Void)?)
}

extension ImageCacheManagerProtocol {
    func setImage(
        imageView: UIImageView,
        path: String?,
        scale: ImageScale? = nil,
        progress: ((_ done: Int64, _ total: Int64) -> Void)? = nil,
        completion: ((_ view: UIImageView, _ image: UIImage?) -> Void)? = nil) {
        self.setImage(
            imageView: imageView,
            path: path,
            scale: scale,
            progress: progress,
            completion: completion)
    }
        
}

class ImageCacheManager: ImageCacheManagerProtocol {
    fileprivate static var imageCache: AutoPurgingImageCache = {
        return AutoPurgingImageCache(memoryCapacity: 100_000_000,
                                     preferredMemoryUsageAfterPurge: 60_000_000)
    }()
    fileprivate static var imageDownloader: ImageDownloader = {
        return ImageDownloader(configuration: ImageDownloader.defaultURLSessionConfiguration(),
                               downloadPrioritization: .fifo,
                               maximumActiveDownloads: 4,
                               imageCache: imageCache)
    }()
    
    private var reception: RequestReceipt? = nil
    private var queue: DispatchQueue = DispatchQueue.global()
    
    // set image
    func setImage(
        imageView: UIImageView,
        path: String?,
        scale: ImageScale?,
        progress: ((_ done: Int64, _ total: Int64) -> Void)?,
        completion: ((_ view: UIImageView, _ image: UIImage?) -> Void)?) {
        
        // request
        self.reception?.request.cancel()
        self.queue.async {
            let downloader: ImageDownloader = ImageCacheManager.imageDownloader
            if let path: String = path,
                let url: URL = URL(string: path) {
                let request: URLRequest = URLRequest(url: url)
                let reception = downloader.download(
                    request,
                    receiptID: path,
                    progress: { (p) in
                        progress?(p.completedUnitCount, p.totalUnitCount)
                }, progressQueue: DispatchQueue.main,
                   completion: { (response) in
                    self.queue.async {
                        var image: UIImage? = response.result.value
                        if let scale = scale {
                            image = scale.scale(image: image)
                        }
                        DispatchQueue.main.async {
                            imageView.image = image
                            completion?(imageView, image)
                        }
                    }
                })
                DispatchQueue.main.async {
                    self.reception = reception
                }
            } else {
                DispatchQueue.main.async {
                    completion?(imageView, nil)
                }
            }
        }
    }
}
