//
//  Storyboards.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol Storyboard {
    static var identifier: String { get }
}

extension Storyboard {
    
    // MARK: Routers
    static func productsRouter(view: ViewProtocol) -> ProductsRouterProtocol {
        return ProductsRouter(view: view)
    }
    
    // MARK: Repos
    static func remoteRepo() -> RemoteRepoProtocol {
        return RemoteRepo(api: Config.api)
    }
    static func localRepo() -> LocalRepoProtocol {
        return LocalRepo()
    }
    
    // MARK: Interactors
    static func productsInteractor() -> ProductsInteractorProtocol  {
        return ProductsInteractor(
            remoteRepo: self.remoteRepo(),
            localRepo: self.localRepo(),
            rrMapper: self.rrMapper(),
            modelMapper: self.modelMapper(),
            lrMapper: self.lrMapper())
    }
    
    // MARK: Mappers
    static func rrMapper() -> RRMapperProtocol {
        return RRMapper()
    }
    static func modelMapper() -> ModelMapperProtocol {
        return ModelMapper()
    }
    static func lrMapper() -> LRMapperProtocol {
        return LRMapper()
    }
}

struct Storyboards { }

// MARK: Storyboard - defaults
extension Storyboard {
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: self.identifier, bundle: nil)
    }
    static func instantiateViewController<T: ViewProtocol>(withIdentifier identifier: String) -> T {
        return self.storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }
}

