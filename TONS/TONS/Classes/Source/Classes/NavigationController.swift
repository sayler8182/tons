//
//  NavigationController.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit
import Hero

class NavigationController: UINavigationController {
    weak var popupViewController: PopupViewController?
    fileprivate lazy var heroHelper: HeroHelper = HeroHelper()
    
    fileprivate weak var backgroundImageView: UIImageView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // config
        self.setTitleColor(color: UIColor.typographyDark)
        self.heroHelper.config(navigationController: self)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        if let presentingViewController = self.presentingViewController as? UINavigationController {
            presentingViewController.topViewController?.viewWillAppear(flag)
        } else {
            self.presentingViewController?.viewWillAppear(flag)
        }
        super.dismiss(animated: flag, completion: completion)
    }
    
    // set transparent bar
    func setTransparentBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
    }
    
    // set title color
    func setTitleColor(color: UIColor) {
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: color]
    }
    
    // set background color
    func setBackgroundColor(color: UIColor) {
        self.view.backgroundColor = color
    }
    
    // set background image
    func setBackgroundImage(image: UIImage?) {
        self.backgroundImageView?.removeFromSuperview()
        
        // image
        if let image = image {
            let imageView = UIImageView(image: image)
            imageView.contentMode = UIViewContentMode.scaleAspectFill
            imageView.insertSubviewAndStreach(in: self.view, at: 0)
            self.backgroundImageView = imageView
        }
    }
}

class HeroHelper: NSObject, UINavigationControllerDelegate, HeroProgressUpdateObserver {
    fileprivate weak var navigationController: NavigationController?
    fileprivate var operation: UINavigationControllerOperation = UINavigationControllerOperation.none
    
    func config(navigationController: NavigationController) {
        self.navigationController = navigationController
        
        navigationController.topViewController?.hero.isEnabled = true
        navigationController.hero.navigationAnimationType = .fade
        navigationController.hero.isEnabled = true
        navigationController.delegate = self
    }
    
    func heroDidUpdateProgress(progress: Double) { }
    
    fileprivate func addEdgePanGesture(to view: UIView) {
        let pan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(popViewController(_:)))
        pan.edges = .left
        view.addGestureRecognizer(pan)
    }
    
    @objc private func popViewController(_ gesture: UIScreenEdgePanGestureRecognizer) {
        guard let view = gesture.view else { return }
        let translation = gesture.translation(in: nil)
        let progress = translation.x / 2 / view.bounds.width
        
        // handle gesture
        switch gesture.state {
        case .began:
            self.navigationController?.topViewController?.hero.dismissViewController()
        case .changed:
            Hero.shared.update(progress)
        default:
            if progress + gesture.velocity(in: nil).x / view.bounds.width > 0.3 {
                Hero.shared.finish()
            } else {
                Hero.shared.cancel()
            }
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return Hero.shared.navigationController(navigationController, interactionControllerFor: animationController)
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .push {
            self.addEdgePanGesture(to: toVC.view)
        }
        self.operation = operation
        Hero.shared.observeForProgressUpdate(observer: self)
        return Hero.shared.navigationController(navigationController, animationControllerFor: operation, from: fromVC, to: toVC)
    }
}
