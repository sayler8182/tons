//
//  PopupViewController.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit
import Hero
import SnapKit

extension PopupViewController {
    enum Insets {
        case `default`
        case half
        case custom(edgeInsets: UIEdgeInsets)
        
        var edgeInsets: UIEdgeInsets {
            switch self {
            case .`default`:                return UIEdgeInsetsMake(35, 0, 0, 0)
            case .half:                     return UIEdgeInsetsMake(UIScreen.main.bounds.height / 2, 0, 0, 0)
            case .custom(let edgeInsets):   return edgeInsets
            }
        }
    }
}

class PopupViewController: UIViewController {
    fileprivate var controller: NavigationController!
    fileprivate var inset: Insets!
    fileprivate lazy var contentView: UIView = self.createContentView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // config
        self.hero.modalAnimationType = .none
        self.view.backgroundColor = UIColor.clear
        
        // dismiss gesture
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // update view
        self.updateView()
    }
    
    // fill with controller
    func fill(with controller: NavigationController, inset: Insets) {
        self.controller = controller
        self.inset = inset
        controller.popupViewController = self
    }
    
    @objc func panGestureAction(_ gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        let progress = translation.y / 2 / self.view.bounds.height
        
        switch gesture.state {
        case .began: break
        case .changed:
            let y = max(0, translation.y)
            self.contentView.snp.updateConstraints { (make) in
                make.top.equalTo(y)
            }
        default:
            let duration: Double = 0.3
            let shouldDismiss: Bool = progress + gesture.velocity(in: nil).y / self.view.bounds.height > 0.3
            let backgroundColor: UIColor = shouldDismiss ? UIColor.clear : UIColor.popupBackground
            let topPosition: CGFloat = shouldDismiss ? self.view.bounds.height : 0
            UIView.animate(
                withDuration: duration,
                animations: {
                    self.view.backgroundColor = backgroundColor
                    self.contentView.snp.updateConstraints { (make) in
                        make.top.equalTo(topPosition)
                    }
                    self.view.layoutIfNeeded()
            }, completion: { status in
                guard shouldDismiss == true else { return }
                self.dismiss(animated: false, completion: nil)
            })
        }
    }
    
    // update view
    fileprivate func updateView() {
        self.controller.view.addSubviewAndStreach(in: self.contentView, edgeInsets: self.inset.edgeInsets)
        
        // animate showing
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.popupBackground
            self.contentView.snp.updateConstraints { (make) in
                make.top.equalTo(0)
            }
            self.view.layoutIfNeeded()
        }
    }
    
    // create contentView
    fileprivate func createContentView() -> UIView {
        let frame: CGRect = self.view.bounds
        let superview = self.view!
        let view = UIView(frame: frame)
        view.backgroundColor = UIColor.clear
        superview.addSubview(view)
        view.snp.makeConstraints { (make) in
            make.leading.equalTo(superview.snp.leading)
            make.top.equalTo(superview.snp.top).offset(frame.height)
            make.trailing.equalTo(superview.snp.trailing)
            make.height.equalTo(frame.height)
        }
        return view
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        let duration: Double = flag ? 0.3 : 0
        let frame: CGRect = UIScreen.main.bounds
        UIView.animate(
            withDuration: duration,
            animations: {
                self.view.backgroundColor = UIColor.clear
                self.contentView.snp.updateConstraints { (make) in
                    make.top.equalTo(frame.height)
                }
                self.view.layoutIfNeeded()
        }, completion: { status in
            super.dismiss(animated: false, completion: completion)
        })
        
        // will appear presenting view
        if let presentingViewController = self.presentingViewController as? UINavigationController {
            presentingViewController.topViewController?.viewWillAppear(flag)
        }
    }
}
