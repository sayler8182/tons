//
//  ViewController.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol ViewControllerProtocol: class, Logable {
    var navigationController: UINavigationController? { get }
}

class ViewController: UIViewController, ViewControllerProtocol, ViewLifeCycleProtocol {
    var lifeCyclePresenter: PresenterProtocol? { return nil }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lifeCyclePresenter?.viewDidLoad()
        
        // set navigation bar style
        self.setNavigationBarStyle()
        
        // round top
        self.roundTop()
    }
    
    deinit {
        print("[DEINIT]: \(type(of: self))")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lifeCyclePresenter?.viewWillAppear()
        
        // set status bar style
        self.setStatusBarStyle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.lifeCyclePresenter?.viewDidAppear()
        
        // set status bar style
        self.setStatusBarStyle()
        
        // set swipe to back
        self.setSwipeToBack()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(false)
        super.viewWillDisappear(animated)
        self.lifeCyclePresenter?.viewWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.lifeCyclePresenter?.viewDidDisappear()
    }
    
    // set status bar style
    func setStatusBarStyle() {
        UIApplication.shared.statusBarStyle = self.lifeCyclePresenter?.statusBarStyle ?? UIStatusBarStyle.default
        UIApplication.shared.isStatusBarHidden = self.lifeCyclePresenter?.statusBarStyle == nil
    }
    
    // set navigation bar style
    fileprivate func setNavigationBarStyle() {
        guard let navigationController = self.navigationController else { return }
        let backImage = UIImage(named: "back")
        navigationController.navigationBar.backIndicatorImage = backImage
        navigationController.navigationBar.backIndicatorTransitionMaskImage = backImage
        let backButton = UIBarButtonItem()
        backButton.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = backButton
    }
    
    // set swipe to back
    func setSwipeToBack() {
        guard let navigationController = self.navigationController else { return }
        let canGoBack = 1 < navigationController.viewControllers.count
        navigationController.interactivePopGestureRecognizer?.isEnabled = canGoBack
        navigationController.interactivePopGestureRecognizer?.delegate = navigationController as? UIGestureRecognizerDelegate
    }
    
    // round top
    func roundTop() {
        guard let roundView = self as? RoundViewProtocol else { return }
        self.view.round(corners: roundView.roundCorners, radius: 8)
    }
}
