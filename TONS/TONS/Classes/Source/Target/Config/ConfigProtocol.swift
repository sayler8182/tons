//
//  ConfigProtocol.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

protocol ConfigProtocol {
    static var api: String { get }
}
