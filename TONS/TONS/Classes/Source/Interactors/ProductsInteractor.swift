//
//  ProductsInteractor.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

protocol ProductsInteractorProtocol {
    init(remoteRepo: RemoteRepoProtocol,
    localRepo: LocalRepoProtocol,
    rrMapper: RRMapperProtocol,
    modelMapper: ModelMapperProtocol,
    lrMapper: LRMapperProtocol)
    
    func products(query: String?,
                  page: Int,
                  success: @escaping ProductsInteractorProtocolProductsSuccess,
                  error: @escaping ProductsInteractorProtocolError)
}

typealias ProductsInteractorProtocolProductsSuccess = (_ products: [Product]) -> Void
typealias ProductsInteractorProtocolError = (_ message: String?) -> Void

class ProductsInteractor: ProductsInteractorProtocol {
    fileprivate let remoteRepo: RemoteRepoProtocol
    fileprivate let localRepo: LocalRepoProtocol
    fileprivate let rrMapper: RRMapperProtocol
    fileprivate let modelMapper: ModelMapperProtocol
    fileprivate let lrMapper: LRMapperProtocol
    
    required init(remoteRepo: RemoteRepoProtocol,
                  localRepo: LocalRepoProtocol,
                  rrMapper: RRMapperProtocol,
                  modelMapper: ModelMapperProtocol,
                  lrMapper: LRMapperProtocol) {
        self.remoteRepo = remoteRepo
        self.localRepo = localRepo
        self.rrMapper = rrMapper
        self.modelMapper = modelMapper
        self.lrMapper = lrMapper
    }
    
    func products(
        query: String?,
        page: Int,
        success: @escaping ProductsInteractorProtocolProductsSuccess,
        error: @escaping ProductsInteractorProtocolError) {
        let endpoint = EndpointsRemoteRepo.Products.products(query: query, page: page)
        self.remoteRepo
            .request(endpoint: endpoint)
            .response(type: Array<RRProductModel>.self) { [weak self] (_error, _model) in
                guard let `self` = self else { return }
                guard let model = _model else {
                    
                    // get cached items
                    if page == 1 &&
                        query?.isEmpty != false {
                        let lrProducts: [LRProductModel] = self.localRepo.objects()
                        let products: [Product] = self.lrMapper.map(lrProducts)
                        success(products)
                    }
                    
                    // error
                    error(_error?.message)
                    return
                }
                let products: [Product] = self.rrMapper.map(model)
                let lrProducts: [LRProductModel] = self.modelMapper.map(products)
                self.localRepo.write(lrProducts)
                success(products)
        }
    }
}
