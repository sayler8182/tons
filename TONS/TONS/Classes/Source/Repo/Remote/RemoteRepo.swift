//
//  RemoteRepo.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import Alamofire

class RemoteRepo: RemoteRepoProtocol {
    var defaultsHeaders: HTTPHeaders {
        var headers: HTTPHeaders = [:]
        headers["App-Version"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        return headers
    }
    
    fileprivate var api: String
    
    init(api: String) {
        self.api = api
    }
    
    func defaultsHeaders(last_update: Int?) -> HTTPHeaders {
        var headers = self.defaultsHeaders
        if let last_update = last_update {
            headers["last_update"] = "\(last_update)"
        }
        return headers
    }
    
    func request(endpoint: EndpointRemoteRepoProtocol) -> Request {
        return self.request(groupHash: String.unique, endpoint: endpoint)
    }
    
    func request(
        groupHash: String,
        endpoint: EndpointRemoteRepoProtocol) -> Request {
        return self.request(groupHash: groupHash, endpoint: endpoint, autoSendRequest: true)
    }
    
    func request(
        groupHash: String,
        endpoint: EndpointRemoteRepoProtocol,
        autoSendRequest: Bool) -> Request {
        
        SessionManager.default.session.configuration.timeoutIntervalForRequest = 20
        
        var dataRequest: DataRequest? = nil
        if autoSendRequest == true {
            endpoint.debug(interface: self.interface)
            dataRequest = Alamofire.request(
                self.interface(endpoint: endpoint),
                method: endpoint.method,
                parameters: endpoint.parameters,
                headers: endpoint.headers)
        }
        
        return self.request(
            groupHash: groupHash,
            dataRequest: dataRequest,
            endpoint: endpoint)
    }
    
    func request(
        groupHash: String,
        dataRequest: DataRequest?,
        endpoint: EndpointRemoteRepoProtocol) -> Request {
        
        // clear get parameters
        var parameters = endpoint.parameters
        for param in parameters {
            guard param.0.hasPrefix("{") && param.0.hasSuffix("}") else { continue }
            parameters.removeValue(forKey: param.key)
        }
        
        return Request(
            groupHash: groupHash,
            endpoint: endpoint,
            dataRequest: dataRequest)
    }
    
    func interface(endpoint: EndpointRemoteRepoProtocol) -> URLConvertible {
        return self.api + endpoint.interface
    }
}
