//
//  ProductsRemoteRepo.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import Alamofire

extension EndpointsRemoteRepo {
    enum Products: EndpointRemoteRepoProtocol {
        case products(query: String?,
            page: Int)
        
        var parameters: Parameters {
            switch self {
            case .products(let query, let page):
                var parameters: Parameters = [
                    "page": page
                ]
                if query?.isEmpty == false {
                    parameters["query"] = query?.replacingOccurrences(of: " ", with: "+")
                }
                return parameters
            }
        }
        var headers: HTTPHeaders {
            switch self {
            case .products:        return [:]
            }
        }
        var interface: String {
            switch self {
            case .products:        return "items"
            }
        }
        var method: HTTPMethod {
            switch self {
            case .products:        return HTTPMethod.get
            }
        }
    }
}
