//
//  RRProduct.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct RRProductModel: RRModelProtocol {
    let gtin14: String
    let brand_name: String?
    let name: String
    let size: String?
    let images: [String]?
    
    let fat: Double?
    let cholesterol: Double?
    let sodium: Double?
    let carbohydrate: Double?
    let protein: Double?
    
    init?(json: JSON) {
        guard let gtin14: String = json["gtin14"].string else { return nil }
        guard let name: String = json["name"].string else { return nil }
        
        self.gtin14 = gtin14
        self.brand_name = json["brand_name"].string
        self.name = name
        self.size = json["size"].string
        self.images = json["images"].array?
            .compactMap { $0["url"].string }
        
        self.fat = json["fat"].string?.double
        self.cholesterol = json["cholesterol"].double
        self.sodium = json["sodium"].double
        self.carbohydrate = json["carbohydrate"].double
        self.protein = json["protein"].double
    }
}

extension Array: RRModelProtocol where Element == RRProductModel {
    init?(json: JSON) {
        let array: [RRProductModel] = json.array?
            .compactMap { RRProductModel(json: $0) } ?? []
        self = array
    }
}
