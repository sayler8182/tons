//
//  LocalRepo.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import RealmSwift

class LocalRepo: LocalRepoProtocol {
    static var wasDebuged: Bool = false
    static func debug() {
        guard wasDebuged == false else { return }
        if let string = Realm.Configuration.defaultConfiguration.fileURL?.absoluteString {
            self.print(string)
        } else {
            self.print("Default realm file not found")
        }
        self.wasDebuged = true
    }
    
    fileprivate var realm: Realm? {
        do {
            LocalRepo.debug()
            return try Realm()
        } catch let error {
            let nsError = error as NSError
            if nsError.code == 10 {
                guard let defaultPath: URL = Realm.Configuration.defaultConfiguration.fileURL else { return nil }
                try? FileManager.default.removeItem(at: defaultPath)
                return self.realm
            }
            self.print("Default realm init failed: ", error)
        }
        return nil
    }
    
    func value<T>(key: LocalRepoKeysProtocol) -> T? {
        let userDefaults = UserDefaults.standard
        return userDefaults.object(forKey: key.rawValue) as? T
    }
    
    func writeValue<T>(key: LocalRepoKeysProtocol, value: T?) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: key.rawValue)
    }
    
    func removeValue(key: LocalRepoKeysProtocol) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(nil, forKey: key.rawValue)
    }
    
    func object<T: Object>() -> T? {
        let key: AnyObject = 0 as AnyObject
        return self.object(key)
    }
    
    func object<T: Object>(_ key: AnyObject?) -> T? {
        guard let key = key else { return nil }
        guard let realm = self.realm else { return nil }
        guard let object = realm.object(ofType: T.self, forPrimaryKey: key) else { return nil }
        return !object.isInvalidated ? object : nil
    }
    
    func object<T: Object>(_ predicate: (T) -> Bool) -> T? {
        guard let realm = self.realm else { return nil }
        return realm.objects(T.self).filter(predicate).filter({ !$0.isInvalidated }).first
    }
    
    func objects<T: Object>() -> [T] {
        guard let realm = self.realm else { return [] }
        return realm.objects(T.self).filter({ !$0.isInvalidated })
    }
    
    func objects<T: Object>(_ predicate: (T) -> Bool) -> [T] {
        guard let realm = self.realm else { return [] }
        return realm.objects(T.self).filter(predicate).filter({ !$0.isInvalidated })
    }
    
    func write<T: Object>(_ object: T?) -> Bool {
        guard let object = object else { return false }
        guard let realm = self.realm else { return false }
        guard !object.isInvalidated else { return false }
        
        do {
            try realm.write {
                realm.add(object, update: true)
            }
            return true
        } catch let error {
            print("Writing failed for ", String(describing: T.self), " with error ", error)
        }
        return false
    }
    
    
    func write<T: Object>(_ objects: [T]?) -> Bool {
        guard let objects = objects else { return false }
        guard let realm = self.realm else { return false }
        let validated = objects.filter({ !$0.isInvalidated })
        do {
            try realm.write {
                realm.add(validated, update: true)
            }
            return true
        } catch let error {
            print("Writing of array failed for ", String(describing: T.self), " with error ", error)
        }
        return false
    }
    
    func update(_ block: () -> ()) -> Bool {
        guard let realm = self.realm else { return false }
        do {
            try realm.write(block)
            return true
        } catch let error {
            print("Updating failed with error ", error)
        }
        return false
    }
    
    func delete<T: Object>(_ object: T) -> Bool {
        guard let realm = self.realm else { return false }
        guard !object.isInvalidated else { return true }
        do {
            try realm.write {
                realm.delete(object)
            }
            return true
        } catch let error {
            print("Writing of array failed for ", String(describing: T.self), " with error ", error)
        }
        return false
    }
    
    func flush() -> Bool {
        UserDefaults.standard.dictionaryRepresentation().keys.forEach { key in
            UserDefaults.standard.removeObject(forKey: key)
        }
        guard let realm = self.realm else { return false }
        
        do {
            // flush
            try realm.write {
                // realm.delete(realm.objects(LRModel.self))
            }
            return true
        } catch let error {
            print("Databse flush failed with ", error)
        }
        return false
    }
}
