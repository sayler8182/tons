//
//  LRProductModel.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import RealmSwift

class LRProductModel: Object, LRModel {
    @objc dynamic var gtin14: String        = ""
    @objc dynamic var brand_name: String?
    @objc dynamic var name: String          = ""
    @objc dynamic var size: String?
    var images: List<String>                = List<String>()
    
    var fat: RealmOptional                  = RealmOptional<Double>()
    var cholesterol: RealmOptional          = RealmOptional<Double>()
    var sodium: RealmOptional               = RealmOptional<Double>()
    var carbohydrate: RealmOptional         = RealmOptional<Double>()
    var protein: RealmOptional              = RealmOptional<Double>()
    
    override class func primaryKey() -> String? {
        return "gtin14"
    }
}
