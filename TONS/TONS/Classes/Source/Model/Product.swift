//
//  Product.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

struct Product: ItemModel {
    let gtin14: String
    let brandName: String?
    let name: String
    let size: String?
    let images: [String]?
    
    let fat: Double?
    let cholesterol: Double?
    let sodium: Double?
    let carbohydrate: Double?
    let protein: Double?
    
    var details: [ProductDetailsItem] {
        return [
            ProductDetailsItem(type: ProductDetailsItemType.size, value: self.size),
            ProductDetailsItem(type: ProductDetailsItemType.fat, value: self.fat?.string),
            ProductDetailsItem(type: ProductDetailsItemType.cholesterol, value: self.cholesterol?.string),
            ProductDetailsItem(type: ProductDetailsItemType.sodium, value: self.sodium?.string),
            ProductDetailsItem(type: ProductDetailsItemType.carbohydrate, value: self.carbohydrate?.string),
            ProductDetailsItem(type: ProductDetailsItemType.protein, value: self.protein?.string)
            ].compactMap { $0 }
    }
}
