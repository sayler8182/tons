//
//  ProductDetailsItem.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

enum ProductDetailsItemType {
    case size
    case fat
    case cholesterol
    case sodium
    case carbohydrate
    case protein
    
    var name: String {
        switch self {
        case .size:         return "Size"
        case .fat:          return "Total fat"
        case .cholesterol:  return "Cholesterol"
        case .sodium:       return "Sodium"
        case .carbohydrate: return "Total carbohydrate"
        case .protein:      return "Protein"
        }
    }
}

struct ProductDetailsItem: ItemModel {
    let type: ProductDetailsItemType
    let value: String
    
    init?(type: ProductDetailsItemType, value: String?) {
        guard let value: String = value else { return nil }
        self.init(type: type, value: value)
    }
    
    init(type: ProductDetailsItemType, value: String) {
        self.type = type
        self.value = value
    }
}
