//
//  RRMapper.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

class RRMapper: Mapper, RRMapperProtocol {
    struct RR { }
    
    required init() {
        super.init()
        
        self.register(RR.ProductMap.self, forType: RRProductModel.self)
        self.register(RR.ProductArrayMap.self, forType: [RRProductModel].self)
    }
}

extension RRMapper.RR {
    class ProductMap: Map<RRProductModel, Product> {
        override func map(_ object: RRProductModel) -> Product {
            let product = Product(
                gtin14: object.gtin14,
                brandName: object.brand_name,
                name: object.name,
                size: object.size,
                images: object.images,
                fat: object.fat,
                cholesterol: object.cholesterol,
                sodium: object.sodium,
                carbohydrate: object.carbohydrate,
                protein: object.protein)
            return product
        }
    }
    class ProductArrayMap: Map<[RRProductModel], [Product]> {
        override func map(_ object: [RRProductModel]) -> [Product] {
            let itemMap: ProductMap = ProductMap()
            return object.map { itemMap.map($0) }
        }
    }
}
