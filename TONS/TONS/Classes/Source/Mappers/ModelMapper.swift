//
//  ModelMapper.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import RealmSwift

class ModelMapper: Mapper, ModelMapperProtocol {
    struct Model { }
    
    required init() {
        super.init()
        
        self.register(Model.ProductMap.self, forType: Product.self)
        self.register(Model.ProductArrayMap.self, forType: [Product].self)
    }
}

extension ModelMapper.Model {
    class ProductMap: Map<Product, LRProductModel> {
        override func map(_ object: Product) -> LRProductModel {
            let product = LRProductModel()
            
            product.gtin14              = object.gtin14
            product.brand_name          = object.brandName
            product.name                = object.name
            product.size                = object.size
            object.images?.forEach {
                product.images.append($0)
            }
            product.fat.value           = object.fat
            product.cholesterol.value   = object.cholesterol
            product.sodium.value        = object.sodium
            product.carbohydrate.value  = object.carbohydrate
            product.protein.value       = object.protein
            
            return product
        }
    }
    class ProductArrayMap: Map<[Product], [LRProductModel]> {
        override func map(_ object: [Product]) -> [LRProductModel] {
            let itemMap: ProductMap = ProductMap()
            return object.map { itemMap.map($0) }
        }
    }
}
