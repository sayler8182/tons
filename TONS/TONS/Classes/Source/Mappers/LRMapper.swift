//
//  LRMapper.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

class LRMapper: Mapper, LRMapperProtocol {
    struct LR { }
    
    required init() {
        super.init()
        
        self.register(LR.ProductMap.self, forType: LRProductModel.self)
        self.register(LR.ProductArrayMap.self, forType: [LRProductModel].self)
    }
}

extension LRMapper.LR {
    class ProductMap: Map<LRProductModel, Product> {
        override func map(_ object: LRProductModel) -> Product {
            let images: [String] = object.images.map { $0 }
            let product = Product(
                gtin14: object.gtin14,
                brandName: object.brand_name,
                name: object.name,
                size: object.size,
                images: images,
                fat: object.fat.value,
                cholesterol: object.cholesterol.value,
                sodium: object.sodium.value,
                carbohydrate: object.carbohydrate.value,
                protein: object.protein.value)
            
            return product
        }
    }
    class ProductArrayMap: Map<[LRProductModel], [Product]> {
        override func map(_ object: [LRProductModel]) -> [Product] {
            let itemMap: ProductMap = ProductMap()
            return object.map { itemMap.map($0) }
        }
    }
}
