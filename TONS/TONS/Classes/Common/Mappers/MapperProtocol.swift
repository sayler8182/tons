//
//  MapperProtocol.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

protocol MapperProtocol: class {
    @discardableResult func register<T>(_ map: MapProtocol, forType type: T.Type) -> MapperProtocol
    func map<T, U>(_ object: T) -> U
}

protocol MapperTypeable {
    static var mapperType: String { get }
}

extension Optional: MapperTypeable {
    static var mapperType: String {
        return String(describing: Wrapped.self)
    }
}

protocol MapProtocol: class, Logable {
    init()
}

class Map<In, Out>: NSObject, MapProtocol {
    override required init() { }
    func map(_ object: In) -> Out {
        fatalError("Map isn't implemented")
    }
}

protocol RRMapperProtocol: MapperProtocol { }
protocol ModelMapperProtocol: MapperProtocol { }
protocol LRMapperProtocol: MapperProtocol { }

class Mapper: MapperProtocol {
    fileprivate var maps: [String: MapProtocol] = [:]
    
    required init() { }
    
    @discardableResult func register<T>(_ mapType: MapProtocol.Type, forType type: T.Type) -> MapperProtocol {
        let map = mapType.init()
        return self.register(map, forType: type)
    }
    
    @discardableResult func register<T>(_ map: MapProtocol, forType type: T.Type) -> MapperProtocol {
        let type = String(describing: T.self)
        self.maps[type] = map
        return self
    }
    
    func map<T, U>(_ object: T) -> U {
        let type = (T.self as? MapperTypeable.Type)?.mapperType ?? String(describing: T.self)
        if let map = self.maps[type] as? Map<T, U> {
            return map.map(object)
        }
        
        fatalError("Couldn't  map: " + String(describing: T.self) + " output " + String(describing: U.self))
    }
}
