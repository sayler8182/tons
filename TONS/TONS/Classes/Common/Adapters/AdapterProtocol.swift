//
//  AdapterProtocol.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol UIList: class {
    var bounds: CGRect { get }
    var frame: CGRect { get }
    func item<T: UIView>(`for` path: IndexPath, type: ItemType, id: String) -> T
    func set(adapter: AdapterProtocol)
    func reload(indexPathes: [IndexPath?])
    func insert(indexPathes: [IndexPath?])
    func reloadData()
    func updates(_ closure: () -> Void)
    
    func transactedReload(indexPathes: [IndexPath?])
    func transactedInsert(indexPathes: [IndexPath?])
}

extension UITableView: UIList {
    func set(adapter: AdapterProtocol) {
        self.dataSource = adapter
        self.delegate = adapter
        adapter.set(list: self)
    }
    
    func register(type: CellProtocol.Type, itemType: ItemType = ItemType.cell) {
        switch itemType {
        case .header:   return self.register(type.nib, forHeaderFooterViewReuseIdentifier: type.reusableIdentifier)
        case .footer:   return self.register(type.nib, forHeaderFooterViewReuseIdentifier: type.reusableIdentifier)
        case .cell:     return self.register(type.nib, forCellReuseIdentifier: type.reusableIdentifier)
        }
    }
    func item<T: UIView>(for path: IndexPath, type: ItemType, id: String) -> T {
        switch type {
        case .header:   return self.dequeueReusableHeaderFooterView(withIdentifier: id) as! T
        case .footer:   return self.dequeueReusableHeaderFooterView(withIdentifier: id) as! T
        case .cell:     return self.dequeueReusableCell(withIdentifier: id, for: path) as! T
        }
    }
    func reload(indexPathes: [IndexPath?]) {
        let indexPathes: [IndexPath] = indexPathes.compactMap { $0 }
        self.beginUpdates()
        self.reloadRows(at: indexPathes, with: UITableViewRowAnimation.automatic)
        self.endUpdates()
    }
    func insert(indexPathes: [IndexPath?]) {
        let indexPathes: [IndexPath] = indexPathes.compactMap { $0 }
        self.beginUpdates()
        self.insertRows(at: indexPathes, with: UITableViewRowAnimation.automatic)
        self.endUpdates()
    }
    
    func updates(_ closure: () -> Void) {
        self.beginUpdates()
        closure()
        self.endUpdates()
    }
    
    func transactedReload(indexPathes: [IndexPath?]) {
        let indexPathes: [IndexPath] = indexPathes.compactMap { $0 }
        self.reloadRows(at: indexPathes, with: UITableViewRowAnimation.automatic)
    }
    
    func transactedInsert(indexPathes: [IndexPath?]) {
        let indexPathes: [IndexPath] = indexPathes.compactMap { $0 }
        self.insertRows(at: indexPathes, with: UITableViewRowAnimation.automatic)
    }
}

extension UICollectionView: UIList {
    func set(adapter: AdapterProtocol) {
        self.dataSource = adapter
        self.delegate = adapter
        adapter.set(list: self)
    }
    
    func register(type: CellProtocol.Type, itemType: ItemType = ItemType.cell) {
        switch itemType {
        case .header:   return self.register(type.nib, forSupplementaryViewOfKind: itemType.value, withReuseIdentifier: type.reusableIdentifier)
        case .footer:   return self.register(type.nib, forSupplementaryViewOfKind: itemType.value, withReuseIdentifier: type.reusableIdentifier)
        case .cell:     return self.register(type.nib, forCellWithReuseIdentifier: type.reusableIdentifier)
        }
    }
    
    func item<T: UIView>(for path: IndexPath, type: ItemType, id: String) -> T {
        switch type {
        case .header:   return self.dequeueReusableSupplementaryView(ofKind: type.value, withReuseIdentifier: id, for: path) as! T
        case .footer:   return self.dequeueReusableSupplementaryView(ofKind: type.value, withReuseIdentifier: id, for: path) as! T
        case .cell:     return self.dequeueReusableCell(withReuseIdentifier: id, for: path) as! T
        }
    }
    
    func reload(indexPathes: [IndexPath?]) {
        let indexPathes: [IndexPath] = indexPathes.compactMap { $0 }
        self.reloadItems(at: indexPathes)
    }
    
    func insert(indexPathes: [IndexPath?]) {
        let indexPathes: [IndexPath] = indexPathes.compactMap { $0 }
        self.insertItems(at: indexPathes)
    }
    
    func updates(_ closure: () -> Void) {
        self.performBatchUpdates(closure, completion: nil)
    }
    
    func transactedReload(indexPathes: [IndexPath?]) {
        let indexPathes: [IndexPath] = indexPathes.compactMap { $0 }
        self.reloadItems(at: indexPathes)
    }
    
    func transactedInsert(indexPathes: [IndexPath?]) {
        let indexPathes: [IndexPath] = indexPathes.compactMap { $0 }
        self.insertItems(at: indexPathes)
    }
}

enum ItemType {
    case cell
    case header
    case footer
    
    var value: String {
        switch self {
        case .cell:     return "cell"
        case .header:   return UICollectionElementKindSectionHeader
        case .footer:   return UICollectionElementKindSectionFooter
        }
    }
    
    init(string: String) {
        switch string {
        case UICollectionElementKindSectionHeader:  self = ItemType.header
        case UICollectionElementKindSectionFooter:  self = ItemType.footer
        default:                                    self = ItemType.cell
        }
    }
}

protocol ItemModel { }

protocol ItemPresenter: class {
    var id: String { get }
    var uniqueID: String  { get }
    var model: ItemModel? { get set }
    
    func size(for list: UIList, model: ItemModel?) -> CGSize 
    func configure(item: UIView)
    func selected()
}

func + (lhs: [ItemPresenter], rhs: [ItemPresenter]) -> [ItemPresenter] {
    var lhs = lhs
    lhs.append(contentsOf: rhs)
    return lhs
}

private struct AssociationKeys {
    static var ItemPresenterUniqueID: UInt8 = 0
    static var ItemPresenterModel: UInt8 = 0
}

extension ItemPresenter {
    var uniqueID: String {
        get {
            guard let string = objc_getAssociatedObject(self, &AssociationKeys.ItemPresenterUniqueID) as? String else {
                let string = String.unique
                objc_setAssociatedObject(self, &AssociationKeys.ItemPresenterUniqueID, string, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                return string
            }
            return string
        }
        set { objc_setAssociatedObject(self, &AssociationKeys.ItemPresenterUniqueID, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN) }
    }
}

func == (lhs: ItemPresenter?, rhs: ItemPresenter?) -> Bool {
    return lhs?.uniqueID == rhs?.uniqueID
}

protocol AdapterProtocol: UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var list: UIList?                           { get }
    var presenters: [ItemPresenter]             { get }
    var models: [ItemModel?]                    { get }
    var shouldHideLastUnderline: Bool           { get set }
    var sectionsCount: Int                      { get }
    
    func reload(presenters: [ItemPresenter?])
    func reload(indexPathes: [IndexPath?])
    func insert(indexPathes: [IndexPath?])
    func reload()
    
    func set(list: UIList?)
    func set(presenters: [ItemPresenter]) 
    
    func isLastOrEmpty(item: UIView, for path: IndexPath) -> Bool
    func indexPathes(presenters: [ItemPresenter?]) -> [IndexPath]
    func indexPath(presenter: ItemPresenter?) -> IndexPath?
    func rowsCount(for section: Int) -> Int
    func size(for path: IndexPath) -> CGSize
    func selected(at path: IndexPath)
    func configure(cell: UIView, at path: IndexPath)
    func item<T: UIView>(for path: IndexPath, with list: UIList, with type: ItemType) -> T
    
    func numberOfSections(in tableView: UITableView) -> Int
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    func numberOfSections(in collectionView: UICollectionView) -> Int
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
}
