//
//  Logable.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

protocol Logable { }

extension Logable {
    static func print(_ items: Any, separator: String = " ") {
        print(items, separator: separator, terminator: "\n")
    }
    
    static func print(_ items: Any ..., separator: String = " ", terminator: String = "\n") {
        let output = items.map { "\($0)" }.joined(separator: separator)
        Swift.print(output, terminator: terminator)
    }
    
    func print(_ items: Any) {
        Self.print(items)
    }
    
    func print(_ items: Any ...) {
        Self.print(items)
    }
}
