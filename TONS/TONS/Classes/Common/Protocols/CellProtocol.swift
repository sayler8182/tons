//
//  CellProtocol.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol CellProtocol {
    static var reusableIdentifier: String { get }
    static var nib: UINib { get }
}
