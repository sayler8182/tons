//
//  ModuleProtocols.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

protocol ViewProtocol: class { }

protocol ViewLifeCycleProtocol: class {
    var lifeCyclePresenter: PresenterProtocol? { get }
}

protocol RouterProtocol: class {
    var view: ViewProtocol! { get set }
    init(view: ViewProtocol!)
}

protocol PresenterProtocol: class {
    var statusBarStyle: UIStatusBarStyle? { get }
    func viewDidLoad()
    func viewWillAppear()
    func viewDidAppear()
    func viewWillDisappear()
    func viewDidDisappear()
}

// MARK: ViewProtocol - defaults
extension ViewProtocol {
    var asController: UIViewController {
        return self as! UIViewController
    }
}

// MARK: PresenterProtocol - defaults
extension PresenterProtocol {
    func viewDidLoad() { }
    func viewWillAppear() { }
    func viewDidAppear() { }
    func viewWillDisappear() { }
    func viewDidDisappear() { }
}
