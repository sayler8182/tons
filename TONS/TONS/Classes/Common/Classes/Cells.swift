//
//  Cells.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

class TableViewCell: UITableViewCell, CellProtocol {
    class var reusableIdentifier: String {
        return String(describing: self)
    }
    class var nib: UINib {
        return UINib(nibName: reusableIdentifier, bundle: Bundle.main)
    }
}

class TableViewHeaderFooterView: UITableViewHeaderFooterView, CellProtocol {
    class var reusableIdentifier: String {
        return String(describing: self)
    }
    class var nib: UINib {
        return UINib(nibName: reusableIdentifier, bundle: Bundle.main)
    }
}

class CollectionViewCell: UICollectionViewCell, CellProtocol {
    class var reusableIdentifier: String {
        return String(describing: self)
    }
    class var nib: UINib {
        return UINib(nibName: reusableIdentifier, bundle: Bundle.main)
    }
}
