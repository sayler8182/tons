//
//  UIView+Extensions.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

extension UIView {
    convenience init(in superview: UIView, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero) {
        self.init(frame: superview.bounds)
        self.addSubviewAndStreach(in: superview, edgeInsets: edgeInsets)
    }
    
    convenience init(in superview: UIView, at position: Int, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero) {
        self.init(frame: superview.bounds)
        self.insertSubviewAndStreach(in: superview, at: position, edgeInsets: edgeInsets)
    }
    
    func layoutView(duration: Double, animated: Bool = true) {
        if animated == true {
            UIView.animate(withDuration: duration, animations: self.layoutIfNeeded)
        } else {
            self.layoutIfNeeded()
        }
    }
    
    func layoutView(duration: Double, animated: Bool = true, action: @escaping () -> Void) {
        if animated == true {
            UIView.animate(withDuration: duration, animations: action)
        } else {
            self.layoutIfNeeded()
        }
    }
    
    func round(corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
    }
    
    func addSubviewAndStreach(in superview: UIView?, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero) {
        superview?.addSubview(self)
        self.snp.streach(in: superview, edgeInsets: edgeInsets)
    }
    
    func insertSubviewAndStreach(in superview: UIView?, at position: Int, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero) {
        superview?.insertSubview(self, at: position)
        self.snp.streach(in: superview, edgeInsets: edgeInsets)
    }
}

extension ConstraintViewDSL {
    func streach(in superview: UIView?, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero) {
        guard let superview = superview else { return }
        self.makeConstraints { (make) in
            make.leading.equalTo(superview.snp.leading).offset(edgeInsets.left)
            make.top.equalTo(superview.snp.top).offset(edgeInsets.top)
            make.trailing.equalTo(superview.snp.trailing).offset(edgeInsets.right)
            make.bottom.equalTo(superview.snp.bottom).offset(edgeInsets.bottom)
        }
    }
}
