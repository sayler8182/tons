//
//  Double+Extensions.swift
//  TONS
//
//  Created by Mac on 31.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

extension Double {
    var string: String {
        return String(self)
    }
}
