//
//  String+Extensions.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import UIKit

extension String: ItemModel { }

extension String {
    static var unique: String {
        return UUID().uuidString
    }
    
    var int: Int? {
        return Int(self)
    }
    
    var double: Double? {
        let string: String = self.replacingOccurrences(of: ",", with: ".")
        return Double(string)
    }
    
    var localized: String {
        return self
    }
    
    var isEmptyOrWhitespace: Bool {
        return self.trimmingCharacters(in: .whitespaces).isEmpty
    }
    
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let attrString = NSAttributedString(
            string: self,
            attributes: [NSAttributedStringKey.font: font])
        let size = attrString.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        return ceil(size.height)
    }
}
