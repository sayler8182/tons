//
//  IndexPath+Extensions.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation

extension IndexPath {
    static let zero: IndexPath = IndexPath(item: 0, section: 0)
}
