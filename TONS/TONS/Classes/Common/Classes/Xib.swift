//
//  Xib.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import UIKit

class Xib: UIView {
    var xib: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadFromNib()
    }
    
    func loadFromNib() {
        let xib = Bundle(for: type(of: self)).loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as! UIView
        xib.frame = self.bounds;
        xib.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.insertSubview(xib, at: 0)
        
        // save reference
        self.xib = xib
        
        // prepare view
        self.prepareView()
    }
    
    // to override in subclass
    func prepareView() { }
    
    // hide keyboard
    func hideKeyboard() {
        self.endEditing(false)
    }
    
    // view did layout subviews
    func viewDidLayoutSubviews() { }
    
    // handle content click
    func handleClick() { }
}
