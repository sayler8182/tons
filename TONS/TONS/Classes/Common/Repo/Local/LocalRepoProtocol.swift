//
//  LocalRepoProtocol.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import RealmSwift

protocol LocalRepoKeysProtocol {
    var rawValue: String { get }
}

protocol LocalRepoProtocol: Logable {
    func value<T>(key: LocalRepoKeysProtocol) -> T?
    func writeValue<T>(key: LocalRepoKeysProtocol, value: T?)
    func removeValue(key: LocalRepoKeysProtocol)
    
    func object<T: Object>() -> T?
    func object<T: Object>(_ key: AnyObject?) -> T?
    func object<T: Object>(_ predicate: (T) -> Bool) -> T?
    
    func objects<T: Object>() -> [T]
    func objects<T: Object>(_ predicate: (T) -> Bool) -> [T]
    
    @discardableResult func write<T: Object>(_ object: T?) -> Bool
    @discardableResult func write<T: Object>(_ objects: [T]?) -> Bool
    
    @discardableResult func update(_ block: () -> ()) -> Bool
    @discardableResult func delete<T: Object>(_ object: T) -> Bool
    
    @discardableResult func flush() -> Bool
}
