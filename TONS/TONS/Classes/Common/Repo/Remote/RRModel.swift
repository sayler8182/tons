//
//  RRModel.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol RRModelProtocol: Codable {
    init?(json: JSON)
}
