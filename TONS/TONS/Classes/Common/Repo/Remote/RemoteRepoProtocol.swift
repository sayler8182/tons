//
//  RemoteRepoProtocol.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias RemoteResponseAction<T: RRModelProtocol>  = (_ error: RemoteError?, _ model: T?) -> Void
typealias RemoteParseAction                         = (_ json: JSON, _ data: JSON) -> Bool

class Request {
    static var activeRequest: Int = 0 {
        didSet { Request.updateActivity() }
    }
    let groupHash: String
    let endpoint: EndpointRemoteRepoProtocol
    let dataRequest: DataRequest?
    
    init(groupHash: String = String.unique, endpoint: EndpointRemoteRepoProtocol, dataRequest: DataRequest?) {
        self.groupHash = groupHash
        self.endpoint = endpoint
        self.dataRequest = dataRequest
    }
    
    @discardableResult
    func response<T: RRModelProtocol>(
        type: T.Type,
        startQueue: DispatchQueue = DispatchQueue.global(qos: .default),
        endQueue: DispatchQueue = DispatchQueue.main,
        response: @escaping RemoteResponseAction<T>) -> Self {
        return self.response(
            type: type,
            startQueue: startQueue,
            endQueue: endQueue,
            response: response,
            parse: { (json, data) -> Bool in
                guard let model = T.init(json: data) else { return false }
                response(nil, model)
                return true
        })
    }
    
    @discardableResult
    func response<T: RRModelProtocol>(
        type: T.Type,
        startQueue: DispatchQueue = DispatchQueue.global(qos: .default),
        endQueue: DispatchQueue = DispatchQueue.main,
        response: @escaping RemoteResponseAction<T>,
        parse: @escaping RemoteParseAction) -> Self {
        Request.activeRequest += 1
        self.dataRequest?.responseJSON(
            queue:startQueue,
            completionHandler: { (_response) in
                _response.response(
                    type: type,
                    startQueue: startQueue,
                    endQueue: endQueue,
                    response: response,
                    parse: parse)
        })
        return self
    }
    
    static func updateActivity() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = activeRequest > 0
        }
    }
}

extension DataResponse where Value == Any {
    func response<T: RRModelProtocol> (
        type: T.Type,
        startQueue: DispatchQueue = DispatchQueue.global(qos: .default),
        endQueue: DispatchQueue = DispatchQueue.main,
        response: @escaping RemoteResponseAction<T>,
        parse: @escaping RemoteParseAction) {
        Request.activeRequest -= 1
        startQueue.async {
            switch self.result {
            case .failure(let _error):
                endQueue.async {
                    response(RemoteError(failure: _error), nil)
                }
            case .success:
                let json = JSON(self.result.value)
                if let _error = RemoteError(json: json) {
                    endQueue.async {
                        response(_error, nil)
                    }
                } else {
                    endQueue.async {
                        guard parse(json, json) == false else { return }
                        response(RemoteError.error(message: "Parse error".localized, code: nil, status: nil), nil)
                    }
                }
            }
        }
    }
}

enum RemoteError: Error {
    case error(message: String?, code: String?, status: Int?)
    case failure
    case cancelled
    
    var message: String? {
        switch self {
        case .error(let message, _, _): return message
        case .failure:                  return "Interner error".localized
        case .cancelled:                return nil
        }
    }
    
    init(failure: Error) {
        if let error = failure as? URLError, error.code == URLError.notConnectedToInternet {
            // internet connection failure
            self = .failure
        } else if let failure = failure as? URLError, failure.code == URLError.cancelled {
            // cancelled
            self = .cancelled
        } else {
            // other failures
            let error = failure as NSError
            self = .error(message: error.description, code: nil, status: nil)
        }
    }
    
    // TODO: Here should be handling API errors
    init?(json: JSON) {
        let errorCode: String = json["description"]["error_code"].string ?? ""
        let errorDescription: String = json["description"]["error_description"].string ?? ""
        let validator: String = json["description"]["validator"].dictionary
            .map { $0.values.compactMap { $0.string } }?
            .joined(separator: "\n") ?? ""
        
        guard !errorCode.isEmptyOrWhitespace ||
            !errorDescription.isEmptyOrWhitespace ||
            !validator.isEmptyOrWhitespace else { return nil }
        
        let status = json["description"]["status"].string?.int
        let message = !errorDescription.isEmptyOrWhitespace ? errorDescription : validator
        self = .error(message: message, code: errorCode, status: status)
    }
}

protocol RemoteRepoProtocol {
    var defaultsHeaders: HTTPHeaders { get }
    
    func defaultsHeaders(last_update: Int?) -> HTTPHeaders
    func request(endpoint: EndpointRemoteRepoProtocol) -> Request
    func request(groupHash: String, endpoint: EndpointRemoteRepoProtocol) -> Request
    func request(groupHash: String, endpoint: EndpointRemoteRepoProtocol, autoSendRequest: Bool) -> Request
    func request(groupHash: String, dataRequest: DataRequest?, endpoint: EndpointRemoteRepoProtocol) -> Request
}

extension JSON {
    init(_ object: Any?) {
        if let object  = object {
            self.init(object)
        } else {
            self.init()
        }
    }
}
