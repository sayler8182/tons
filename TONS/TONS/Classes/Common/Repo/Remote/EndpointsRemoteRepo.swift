//
//  EndpointsRemoteRepo.swift
//  TONS
//
//  Created by Mac on 30.07.2018.
//  Copyright © 2018 Limbo. All rights reserved.
//

import Foundation
import Alamofire

protocol EndpointsRemoteRepoProtocol { }

protocol EndpointRemoteRepoProtocol: Logable {
    var interface: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders { get }
    var parameters: Parameters { get }
}

extension EndpointRemoteRepoProtocol {
    func debug(interface: ((_ endpoint: EndpointRemoteRepoProtocol) -> URLConvertible)? = nil) {
        let stringHeaders = self.headers
            .map{ "\($0.key): \($0.value)" }
            .joined(separator: "\t")
        let stringParameters = self.parameters
            .map { "\($0.key): \($0.value)" }
            .joined(separator: "\t")
        self.print("")
        if let interface: URLConvertible = interface?(self) {
            print("     \(interface)")
        }
        self.print("     [\(self.method.rawValue.uppercased())] ")
        if !stringHeaders.isEmpty {
            self.print("     \(stringHeaders)")
        }
        if !stringParameters.isEmpty {
            self.print("     \(stringParameters)")
        }
        self.print("")
    }
}

struct EndpointsRemoteRepo: EndpointsRemoteRepoProtocol { }
